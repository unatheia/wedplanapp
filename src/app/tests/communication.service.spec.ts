import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpRequest, HttpParams } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CommunicationService } from '../services/communication.service';

describe('CommunicationService', () => {
  let url = "http://localhost:4200";
  let feedback = {
    name: "John",
    email: "john@email.com",
    message: "123"
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [CommunicationService]
    });
  });

  it('should be created', inject([CommunicationService], (service: CommunicationService) => {
    expect(service).toBeTruthy();
  }));

  it(`should issue a POST request`, inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {
      http.post(url, feedback).subscribe();
      backend.expectOne({
        url: url,
        method: 'POST'
      });
    })
  );

});
