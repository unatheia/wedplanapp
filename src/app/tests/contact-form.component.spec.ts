import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContactFormComponent } from '../components/contact-form/contact-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

describe('ContactFormComponent', () => {
 let component: ContactFormComponent;
 let fixture: ComponentFixture<ContactFormComponent>;
 beforeEach(async(() => {
   TestBed.configureTestingModule({
     imports: [ReactiveFormsModule, HttpClientModule],
     declarations: [ ContactFormComponent ]
   })
   .compileComponents();
 }));
 beforeEach(() => {
   fixture = TestBed.createComponent(ContactFormComponent);
   component = fixture.componentInstance;
   fixture.detectChanges();
 });
 it('should be created', () => {
   expect(component).toBeTruthy();
 });
});
