import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { TileDecoratorComponent } from './components/tile-decorator/tile-decorator.component';
import { PresentationComponent } from './components/presentation/presentation.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ContactInfoComponent } from './components/contact-info/contact-info.component';
import { LocationComponent } from './components/location/location.component';

@NgModule({
  declarations: [
    AppComponent,
    TileDecoratorComponent,
    PresentationComponent,
    ContactFormComponent,
    ContactInfoComponent,
    LocationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD4mVPO8PHHQQeQ1akZHenuJN-M3yhONc4'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
