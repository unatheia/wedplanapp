import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Feedback } from '../interfaces/Feedback';

@Injectable()
export class CommunicationService {

  constructor(private http: HttpClient) { }

  postFeedback(feedback: Feedback) {
    return this.http.post('http://localhost:4200', feedback);
  }
}
