import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tile-decorator',
  template: `
    <div class="tile-wrapper">
      <div class="tile-icon"><i class="material-icons">{{tileIcon}}</i></div>
      <h4 class="tile-title">{{tileTitle}}</h4>
      <div class="tile-content">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  host: {'class': 'tile-bg'},
  styleUrls: ['./tile-decorator.component.scss']
})
export class TileDecoratorComponent implements OnInit {
  @Input("tileIcon") tileIcon: string;
  @Input("tileTitle") tileTitle: string;

  constructor() {}

  ngOnInit() {}

}
