import { Component } from '@angular/core';

@Component({
  selector: 'app-presentation',
  template: `
    <div class="tile-wrapper">
      <h2>{{title}}</h2>
      <p>{{leadPar}}</p>
      <p>{{copyPar}}</p>
    </div>
  `,
  host: {'class': 'tile-bg'}
})

export class PresentationComponent {
  private title: string = 'Wedding planner agency';
  private leadPar: string = `Proin et fermentum odio. Aenean luctus tellus a orci egestas, in auctor ante euismod.
    Aenean tincidunt malesuada urna, ac gravida urna varius sit amet.
    Ut odio ante, venenatis et arcu non, auctor dictum nibh.
    Donec a mattis lorem. Phasellus accumsan metus vitae elit laoreet lobortis. Phasellus sodales eget est ut sollicitudin.`
  private copyPar: string = `Integer posuere aliquam lacus nec iaculis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  Mauris enim ex, consectetur ut posuere maximus, dictum quis dolor. `

  constructor() { }

}
