import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgControl } from '@angular/forms';
import { Feedback } from '../../interfaces/Feedback';
import { CommunicationService } from '../../services/communication.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
  providers: [CommunicationService]
})
export class ContactFormComponent implements OnInit {
  private feedbackForm: FormGroup;
  private submitPending: boolean = false;
  private submitted: boolean = false;

  constructor(private fb: FormBuilder, private communicationService: CommunicationService) { }

  ngOnInit() {
    this.feedbackForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      message: ['', Validators.minLength(2)],
    });
  }

  sendFeedback(model: Feedback, isValid: boolean) {
    this.submitted = true;
    if (isValid) {
      this.submitPending = true;
      this.communicationService.postFeedback(model).subscribe(
        //will error as there's no response
        data => {},
        error => {
          this.submitPending = false;
          this.submitted = false;
          this.feedbackForm.reset();
        }
      );
    }
  }

}
