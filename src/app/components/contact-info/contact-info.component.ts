import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-info',
  template: `
    <div class="contact-par">
      <div class="contact-icon">
        <i class="material-icons">phone</i>
      </div>
      <div class="contact-copy">
        <p>+40 789 789 789</p>
      </div>
    </div>

    <div class="contact-par">
      <div class="contact-icon">
        <i class="material-icons">local_bar</i>
      </div>
      <div class="contact-copy">
        <p>1 Magheru</p>
        <p>Bucharest, the Legendary City</p>
        <p>ROMANIA, the Great Country</p>
      </div>
    </div>
  `,
  styleUrls: ['./contact-info.component.scss']
})
export class ContactInfoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
