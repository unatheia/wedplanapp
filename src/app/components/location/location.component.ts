import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-location',
  template: `
    <agm-map [latitude]="lat" [longitude]="lng" [zoom]="zoom">
    <agm-marker [latitude]="lat" [longitude]="lng"></agm-marker>
    </agm-map>
  `,
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  lat: number = 44.440731;
  lng: number = 26.0996324;
  zoom: number = 14;

  constructor() { }

  ngOnInit() {
  }

}
