import { browser, by, element } from 'protractor';

export class WedPlanAppPage {
  navigateTo() {
    return browser.get('/');
  }

  getAppText() {
    return element(by.css('app-root h2')).getText();
  }
}
