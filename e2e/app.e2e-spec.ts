import { WedPlanAppPage } from './app.po';

describe('wed-plan-app App', () => {
  let page: WedPlanAppPage;

  beforeEach(() => {
    page = new WedPlanAppPage();
  });

  it('should display app title', () => {
    page.navigateTo();
    expect(page.getAppText()).toEqual('Wedding planner agency');
  });
});
